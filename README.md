![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/revkusz/orafor)
# ORAFOR

Original Recipe App For Original Recipes

## Build és futtatás

A front end project build
`ng build`
ekkor a dist mappába fogja le buildelni a front endet

A backend-nek nincsen szüksége buildre. Viszon a server/public mappába bekell másolnunk a front end build eredményét.
Majd NODE_ENV=production környezeti változó beállítása szükséges ahhoz hogy ne a teszt forrásokat használjuk
Futtatni az alkalmazást a
`npm run start`
parancsal lehet.
note: prod környezetben a node.js alkalmazásnak szüksége van egy mongodb szerverre. Teszt környezetben in memory db-t használ


