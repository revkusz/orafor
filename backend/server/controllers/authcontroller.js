const UserModel = require("../models/usermodel");
const jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split(" ")[1];
    const data = jwt.verify(token, process.env.ACC_TOKEN_SECRET);
    try {
      const user = await UserModel.findOne({
        _id: data._id,
        "tokens.token": token,
      });

      if (!user) throw new Error("Not logged in/user does not exist");

      req.user = user;
      req.token = token;
      next();
    } catch (e) {
      res.status(401).send("Not authorized");
    }
  } else {
    res.status(401).send("Not authorized");
  }
};

module.exports = auth;
