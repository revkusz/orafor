const express = require("express");
const RecipeModel = require("../models/recipemodel");
const auth = require("../controllers/authcontroller");

const router = express.Router();

//Add new recipe
router.post("/add", async (req, res) => {
  try {
    const newRecipe = new RecipeModel(req.body);
    const result = await newRecipe.save();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get all recipes
router.get("/get", async (req, res) => {
  try {
    const allRecipes = await RecipeModel.find().populate("createdBy").exec();
    res.send(allRecipes);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Get recipes for userID
router.get("/get/:id", async (req, res) => {
  try {
    const recipes = await RecipeModel.find({ createdBy: req.params.id })
      .populate("createdBy")
      .exec();
    res.send(recipes);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Edit recipe with given id and return the new edited recipe
router.put("/edit/:id", async (req, res) => {
  try {
    const recipe = await RecipeModel.findByIdAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    ).exec();
    res.send(recipe);
  } catch (err) {
    res.status(500).send(err);
  }
});

//Delete recipe with given id and return the new deleted recipe
router.delete("/remove/:id", auth, async (req, res) => {
  try {
    const recipe = await RecipeModel.deleteOne({ _id: req.params.id }).exec();
    res.send(recipe);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
