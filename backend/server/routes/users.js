const express = require("express");
const UserModel = require("../models/usermodel");
const auth = require("../controllers/authcontroller");

const router = express.Router();

//Add new user
router.post("/register", async (req, res) => {
  try {
    var newUser = new UserModel({
      name: req.body.username,
    });

    newUser.password = newUser.hashPw(req.body.password);
    await newUser.save();
    const token = await newUser.generateAuthToken();
    res.status(201).send({ newUser, token });
  } catch (err) {
    res.status(500).send(err);
  }
});

//Login
router.post("/login", async (req, res) => {
  try {
    const username = req.body.username;
    const password = req.body.password;

    const user = await UserModel.findOne({
      name: username,
    }).exec();

    if (!user) {
      res.status(420).send("No such user");
      return;
    }

    if (user.checkPw(password)) {
      const token = await user.generateAuthToken();
      res.send({ user, token });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});

// Log user out of all devices
router.post("/logout", auth, async (req, res) => {
  try {
    req.user.tokens.splice(0, req.user.tokens.length);
    await req.user.save();
    res.send();
  } catch (error) {
    res.status(500).send(error);
  }
});

//Get currently logged in user
router.get("/me", auth, async (req, res) => {
  res.send(req.user);
});

//Get user data by ID
router.get("/:id", async (req, res) => {
  try {
    const userID = req.params.id;
    const users = await UserModel.findById(userID).exec();
    res.send(users);
  } catch (err) {
    res.status(500).send(err);
  }
});

//List all users
router.get("/", async (req, res) => {
  try {
    const users = await UserModel.find().exec();
    res.send(users);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
