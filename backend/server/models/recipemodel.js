const mongoose = require("mongoose");

//Define a schema
const Schema = mongoose.Schema;

const RecipeModelSchema = new Schema({
  createdBy: { type: Schema.Types.ObjectId, ref: "UserModel" },
  name: String,
  instructions: String,
  votes: { type: Number, default: 0 },
  difficulty: { type: Number, default: 1 },
  creationDate: { type: Date, default: Date.now },
  image: { data: Buffer, contentType: String },
});

RecipeModelSchema.set("toJSON", {
  transform: (_doc, { __v, ...rest }, _options) => rest,
});

module.exports = mongoose.model("RecipeModel", RecipeModelSchema);
