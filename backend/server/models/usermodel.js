const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

//Define a schema
var Schema = mongoose.Schema;

var UserModelSchema = new Schema({
  name: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  memberSince: { type: Date, default: Date.now },
  image: { data: Buffer, contentType: String },
  tokens: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],
});

UserModelSchema.methods.generateAuthToken = async function () {
  // Generate an auth token for the user
  const user = this;
  const token = jwt.sign({ _id: user._id }, process.env.ACC_TOKEN_SECRET);
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
};

UserModelSchema.methods.hashPw = function (pw) {
  return bcrypt.hashSync(pw, bcrypt.genSaltSync(10));
};

UserModelSchema.methods.checkPw = function (pw) {
  return bcrypt.compareSync(pw, this.password);
};

UserModelSchema.set("toJSON", {
  transform: (_doc, { __v, password, tokens, ...rest }, _options) => rest,
});

module.exports = mongoose.model("UserModel", UserModelSchema);
