const request = require("supertest");
const app = require("../index");

////////// DEMO TESZTEK //////////
describe("Demo útvonal tesztelése", () => {
  test("Vissza kellene térnie egy GET metódussal", done => {
    request(app)
      .get("/demo/hello/david")
      .then(response => {
        expect(response.statusCode).toBe(200);
      });
      done();
  });
});

describe("Demo útvonal tesztelése", () => {
  test("Vissza kellene térnie a 'Hello david!' szöveggel", done => {
    request(app)
      .get("/demo/hello/david")
      .then(response => {
        expect(response.text).toBe("Hello david!");
      });
      done();
  });
});

describe("Demo útvonal tesztelése", () => {
  test("Vissza kellene térnie egy Get metódussal ami hibás", done => {
    request(app)
      .get("/demo/hello/")
      .then(response => {
        expect(response.statusCode).toBe(404);
      });
      done();
  });
});
////////// DEMO TESZTEK //////////

////////// FELHASZNÁLÓ TESZTEK //////////
describe("Felhasználóval kapcsolatos tesztek", () => {
  test("Létrehozás előtt nem szabad, hogy akármi is visszatérjen a GET metódussal", done => {
    request(app)
      .get("/users/")
      .then(response => {
        expect(Array.from(response.body).length).not.toBeGreaterThan(0);
        done();
      });
  });
});

let firstUserId = 0;
describe("Felhasználóval kapcsolatos tesztek", () => {
  test("Új felhasználó létrehozása", done => {
    request(app)
      .post("/users/register/")
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        "username": "David",
        "password": "Tesztelek"
      })
      .then(response => {
        expect(response.statusCode).toBe(201);
        firstUserId = response.body.newUser._id;
      });
      done();
  });
});

describe("Felhasználóval kapcsolatos tesztek", () => {
  test("Előzőleg egy felhasználó létre lett hozva, megnézzük, hogy a paraméterek helyesen át lettek-e adva", done => {
    request(app)
      .get("/users/")
      .then(response => {
        expect(Array.from(response.body)[0].name).toBe("David");
        done();
      });
  });
});
////////// FELHASZNÁLÓ TESZTEK //////////


////////// RECEPT TESZTEK //////////
describe("Receptekkel kapcsolatos tesztek", () => {
  test("Létrehozás előtt nem szabad, hogy akármi is visszatérjen a GET metódussal", done => {
    request(app)
      .get("/recipes/get/")
      .then(response => {
        expect(Array.from(response.body).length).not.toBeGreaterThan(0);
        done();
      });
  });
});

let firstReceipeId = 0;
describe("Receptekkel kapcsolatos tesztek", () => {
  test("Létre kellene hoznia az első új receptet", done => {
    request(app)
      .post("/recipes/add/")
      .then(response => {
        expect(response.statusCode).toBe(200);
        firstReceipeId = response.body._id;
        done();
      });
  });
});

describe("Receptekkel kapcsolatos tesztek", () => {
  test("Létre kellene hoznia a második új receptet", done => {
    request(app)
      .post("/recipes/add/")
      .send({
        "createdBy": firstUserId
      })
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });
});

describe("Receptekkel kapcsolatos tesztek", () => {
  test("A receptek GET kérésénél összesen kettő receptet kellene, hogy visszakapjunk", done => {
    request(app)
      .get("/recipes/get/")
      .then(response => {
        expect(Array.from(response.body).length).toEqual(2);
        done();
      });
  });
});

describe("Receptekkel kapcsolatos tesztek", () => {
  test("Recept megkeresése felhasználó id alapján", done => {
    request(app)
      .get("/recipes/get/" + firstUserId)
      .then(response => {
        expect(response.body[0].createdBy._id).toBe(firstUserId);
        done();
      });
  });
});

describe("Receptekkel kapcsolatos tesztek", () => {
  test("Recept törlése a recept id alapján autentikáció nélkül", done => {
    request(app)
      .delete("/recipes/remove/" + firstReceipeId)
      .then(response => {
        expect(response.statusCode).toBe(401);
        done();
      });
  });
});

describe("Receptekkel kapcsolatos tesztek", () => {
  test("Recept megkeresése felhasználó id alapján (már nem szabad, hogy visszatérjen adattal)", done => {
    request(app)
      .get("/recipes/get/" + firstReceipeId)
      .then(response => {
        expect(response.body.length).toBe(0);
        done();
      });
  });
});
////////// RECEPT TESZTEK //////////

afterAll(() => setTimeout(() => process.exit(0), 1000))