const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbUtil = require("./util/dbUtil");

const app = express();

//Middleware
app.use(bodyParser.json());
app.use(cors());

const helloRoute = require("./routes/demo");
const recipesRoute = require("./routes/recipes");
const usersRoute = require("./routes/users");

app.use("/demo", helloRoute);
app.use("/recipes", recipesRoute);
app.use("/users", usersRoute);

//Handle production stuff
if (process.env.NODE_ENV === "production") {
  //Static folder
  app.use(express.static(__dirname + "/public"));

  dbUtil.connectToRealDB(process.env.MONGODB_URI);

  //SPA
  app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"));
}
//Local/test environment
else {
  dbUtil.connectToInMemoryDB();
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));

process.on("exit", function () {
  console.log(`Server stopping...`);
  dbUtil.clearDatabase();
  dbUtil.closeDatabase();
});

module.exports = app;
