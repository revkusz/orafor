const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");

var mongod = null;

/**
 * Connect to the in-memory database.
 */
module.exports.connectToInMemoryDB = async () => {
  mongod = new MongoMemoryServer();
  const uri = await mongod.getConnectionString();
  await connect(uri);
};

/**
 * Connect to the real mongoDB instance
 */
module.exports.connectToRealDB = async (uri) => {
  await connect(uri);
};

async function connect(uri) {
  try {
    const mongooseOpts = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    mongoose.set("useCreateIndex", true);

    await mongoose.connect(uri, mongooseOpts);
  } catch (err) {
    console.log("DB connection error: " + err);
  }
}

/**
 * Drop database, close the connection and stop mongod.
 */
module.exports.closeDatabase = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongod.stop();
};

/**
 * Remove all the data for all db collections.
 */
module.exports.clearDatabase = async () => {
  const collections = mongoose.connection.collections;

  for (const key in collections) {
    const collection = collections[key];
    await collection.deleteMany();
  }
};
