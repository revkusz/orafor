import { Injectable } from '@angular/core';
import { isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from './models/Recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  apiUrl = isDevMode() ? 'http://localhost:5000/recipes/' : 'https://orafor.herokuapp.com/recipes/';

  constructor(private http: HttpClient) { }

  getRecipes() {
    return this.http.get<Recipe[]>(this.apiUrl + 'get');
  }

  addRecipe(recipe: any) {
    return this.http.post<any>(this.apiUrl + 'add', {
      name: recipe && recipe.name || '',
      createdBy: '5e91ecb89dad4514f036f022',
      instructions: recipe && recipe.instructions || '',
      difficulty: recipe && recipe.difficulty || 5
    });
  }
}
