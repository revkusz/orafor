import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileToggleService {

  private toggleSource = new BehaviorSubject<boolean>(false);
  public currentToggle = this.toggleSource.asObservable();

  constructor() { }

  public toggelBoolean(newValue: boolean) {
    this.toggleSource.next(newValue);
  }
}
