import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-loginmodal',
  templateUrl: './loginmodal.component.html',
  styleUrls: ['./loginmodal.component.css']
})
export class LoginmodalComponent implements OnInit {
  userName: string;
  password: string;

  loginFailed = false;
  loggedIn: boolean;
  buttonText: string;

  constructor(private modalService: NgbModal, private loginService: AuthenticationService) {
    this.loggedIn = !!localStorage.getItem('oraforToken');
    this.buttonText = this.loggedIn ? 'Logout' : 'Login';
   }

  ngOnInit(): void {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  doLoginAction() {
    this.loginService.login(this.userName, this.password).subscribe((data) => {
      if (localStorage.getItem('oraforToken')) {
        this.modalService.dismissAll();
        this.buttonText = 'Logout';
      }
    });

    if (!localStorage.getItem('oraforToken')) {
      this.loginFailed = true;
    }
  }

  doLogoutAction() {
    this.loginService.logout();
    this.buttonText = 'Login';
  }

  buttonAction(content) {
    if (localStorage.getItem('oraforToken')) {
      this.doLogoutAction();
    } else {
      this.open(content);
    }
  }
}
