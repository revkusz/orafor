import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Recipe } from '../../models/Recipe';
import { RecipesService } from '../../recipes.service';

@Component({
  selector: 'app-profile-tab',
  templateUrl: './profile-tab.component.html',
  styleUrls: ['./profile-tab.component.css']
})
export class ProfileTabComponent implements OnInit {
  closeResult: Recipe;
  recipeName: string;
  recipeSteps: string;
  difficulty = 5;

  loggedIn = localStorage.getItem('oraforToken');

  value: number;

  constructor(private modalService: NgbModal, private recipeService: RecipesService) { }

  ngOnInit(): void {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.recipeService.addRecipe({
        name: result.recipeName,
        instructions: result.recipeSteps,
        difficulty: result.difficulty
      }).subscribe(() => {

      });
    }, (reason) => {
      this.closeResult = reason;
    });
  }
}
