import { Component, OnInit } from '@angular/core';
import { ProfileToggleService } from '../profile-toggle.service';

@Component({
  selector: 'app-tab-component',
  templateUrl: './tab-component.component.html',
  styleUrls: ['./tab-component.component.css']
})
export class TabComponentComponent implements OnInit {

  constructor(private pToggleService: ProfileToggleService) { }

  ngOnInit(): void {
  }

  showProfile() {
    this.pToggleService.toggelBoolean(true);
  }

  showRecipes() {
    this.pToggleService.toggelBoolean(false);
  }

}
