import { Component, OnInit } from '@angular/core';
import { ProfileToggleService } from '../profile-toggle.service';

@Component({
  selector: 'app-view-toggle',
  templateUrl: './view-toggle.component.html',
  styleUrls: ['./view-toggle.component.css']
})
export class ViewToggleComponent implements OnInit {

  public showProfile: boolean;

  public toggle(showProfile: boolean): void {
  }

  constructor(private pToggleService: ProfileToggleService) { }

  ngOnInit(): void {
    this.pToggleService.currentToggle.subscribe(val => this.showProfile = val);
  }

}
