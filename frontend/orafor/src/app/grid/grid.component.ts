import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RecipesService } from '../../recipes.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Recipe } from '../../models/Recipe';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  constructor(private modalService: NgbModal, private recipeService: RecipesService) {}

  @ViewChild('content') content: ElementRef;

  displayedColumns: string[] = ['recipeName', 'difficulty', 'votes'];
  dataSource;
  modalRecipeName: string;
  modalRecipeIstructions: string;
  modalRecipeDifficulty: number;


  onRowClick(row: Recipe) {
    this.modalRecipeName = row.name;
    this.modalRecipeIstructions = row.instructions;
    this.modalRecipeDifficulty = row.difficulty;
    this.open(this.content);
  }

  ngOnInit() {
    return this.recipeService.getRecipes()
      .subscribe(data => this.dataSource = data);
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }
}
