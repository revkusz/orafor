import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabComponentComponent } from './tab-component/tab-component.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { GridComponent } from './grid/grid.component';
import { MatTableModule } from '@angular/material/table';
import { ProfileTabComponent } from './profile-tab/profile-tab.component';
import { ViewToggleComponent } from './view-toggle/view-toggle.component';
import { RecipesService } from '../recipes.service';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { LoginmodalComponent } from './loginmodal/loginmodal.component';

@NgModule({
  declarations: [
    AppComponent,
    TabComponentComponent,
    GridComponent,
    ProfileTabComponent,
    ViewToggleComponent,
    LoginmodalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatTableModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [RecipesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
