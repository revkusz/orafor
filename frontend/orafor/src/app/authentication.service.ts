import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  apiUrl = isDevMode() ? 'http://localhost:5000/users/login' : 'users/login';

  login(username: string, password: string) {
      return this.http.post<any>(this.apiUrl, { username, password })
          .pipe(map(user => {
              if (user) {
                  localStorage.setItem('oraforToken', user.token);
              }

              return user;
          }));
  }

  logout() {
      // remove user from local storage to log user out
      localStorage.removeItem('oraforToken');
  }
}
