export class Recipe {
  createdBy: string;
  name: string;
  instructions: string;
  votes: number;
  difficulty: number;
  creationDate: Date;
  image: string;
}
